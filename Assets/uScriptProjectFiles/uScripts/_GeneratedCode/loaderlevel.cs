//uScript Generated Code - Build 1.0.2522
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class loaderlevel : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_1 = UnityEngine.KeyCode.JoystickButton1;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_1 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_3 = UnityEngine.KeyCode.JoystickButton2;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_3 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_3 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_3 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_5 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_5 = "sunsettime";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_5 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_5 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_7 = "Nighttime";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_7 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_7 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_7 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = null;
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_0;
                  component.uScriptLateStart += Instance_uScriptLateStart_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_0;
               component.uScriptLateStart -= Instance_uScriptLateStart_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_5.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.SetParent(g);
   }
   public void Awake()
   {
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_5.Loaded += uScriptAct_LoadLevel_Loaded_5;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.Loaded += uScriptAct_LoadLevel_Loaded_7;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_5.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_5.Loaded -= uScriptAct_LoadLevel_Loaded_5;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.Loaded -= uScriptAct_LoadLevel_Loaded_7;
   }
   
   void Instance_uScriptStart_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_0( );
   }
   
   void Instance_uScriptLateStart_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_0( );
   }
   
   void uScriptAct_LoadLevel_Loaded_5(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_5( );
   }
   
   void uScriptAct_LoadLevel_Loaded_7(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_7( );
   }
   
   void Relay_uScriptStart_0()
   {
      Relay_In_1();
      Relay_In_3();
   }
   
   void Relay_uScriptLateStart_0()
   {
   }
   
   void Relay_In_1()
   {
      {
         {
         }
      }
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.In(logic_uScriptAct_OnInputEventFilter_KeyCode_1);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1.KeyUp;
      
      if ( test_0 == true )
      {
         Relay_In_5();
      }
   }
   
   void Relay_In_3()
   {
      {
         {
         }
      }
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3.In(logic_uScriptAct_OnInputEventFilter_KeyCode_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3.KeyUp;
      
      if ( test_0 == true )
      {
         Relay_In_7();
      }
   }
   
   void Relay_Loaded_5()
   {
   }
   
   void Relay_In_5()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_5.In(logic_uScriptAct_LoadLevel_name_5, logic_uScriptAct_LoadLevel_destroyOtherObjects_5, logic_uScriptAct_LoadLevel_blockUntilLoaded_5);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Loaded_7()
   {
   }
   
   void Relay_In_7()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_7.In(logic_uScriptAct_LoadLevel_name_7, logic_uScriptAct_LoadLevel_destroyOtherObjects_7, logic_uScriptAct_LoadLevel_blockUntilLoaded_7);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
}
