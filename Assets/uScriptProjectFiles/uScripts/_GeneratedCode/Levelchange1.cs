//uScript Generated Code - Build 1.0.2522
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class Levelchange1 : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_1 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_1 = UnityEngine.KeyCode.JoystickButton0;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_1 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_1 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_2 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_2 = UnityEngine.KeyCode.JoystickButton1;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_2 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_2 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_2 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_3 = UnityEngine.KeyCode.JoystickButton2;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_3 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_3 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_3 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_4 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_4 = "Noontime";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_4 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_4 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_4 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_5 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_5 = "sunsettime";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_5 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_5 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_6 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_6 = "Nighttime";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_6 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_6 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_6 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = null;
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   public void Awake()
   {
   }
   
   public void Start()
   {
   }
   
   public void OnEnable()
   {
   }
   
   public void OnDisable()
   {
   }
   
   public void Update()
   {
   }
   
   public void OnDestroy()
   {
   }
   
}
