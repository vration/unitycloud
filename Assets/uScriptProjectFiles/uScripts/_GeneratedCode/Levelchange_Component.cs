//uScript Generated Code - Build 1.0.2522
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// This is the component script that you should assign to GameObjects to use this graph on them. Use the uScript/Graphs section of Unity's "Component" menu to assign this graph to a selected GameObject.

[AddComponentMenu("uScript/Graphs/Levelchange")]
public class Levelchange_Component : uScriptCode
{
   #pragma warning disable 414
   public Levelchange ExposedVariables = new Levelchange( ); 
   #pragma warning restore 414
   
   
   void Awake( )
   {
   }
   void Start( )
   {
   }
   void OnEnable( )
   {
   }
   void OnDisable( )
   {
   }
   void Update( )
   {
   }
   void OnDestroy( )
   {
   }
   #if UNITY_EDITOR
      void OnDrawGizmos( )
      {
      }
   #endif
}
