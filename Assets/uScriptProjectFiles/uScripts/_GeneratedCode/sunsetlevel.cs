//uScript Generated Code - Build 1.0.2522
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("Untitled", "")]
public class sunsetlevel : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   
   //owner nodes
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_0 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_0 = "Noontime";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_0 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_0 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_0 = true;
   //pointer to script instanced logic node
   uScriptAct_LoadLevel logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1 = new uScriptAct_LoadLevel( );
   System.String logic_uScriptAct_LoadLevel_name_1 = "Nighttime";
   System.Boolean logic_uScriptAct_LoadLevel_destroyOtherObjects_1 = (bool) true;
   System.Boolean logic_uScriptAct_LoadLevel_blockUntilLoaded_1 = (bool) true;
   bool logic_uScriptAct_LoadLevel_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_3 = UnityEngine.KeyCode.JoystickButton0;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_3 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_3 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_3 = true;
   //pointer to script instanced logic node
   uScriptAct_OnInputEventFilter logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_4 = new uScriptAct_OnInputEventFilter( );
   UnityEngine.KeyCode logic_uScriptAct_OnInputEventFilter_KeyCode_4 = UnityEngine.KeyCode.JoystickButton2;
   bool logic_uScriptAct_OnInputEventFilter_KeyDown_4 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyHeld_4 = true;
   bool logic_uScriptAct_OnInputEventFilter_KeyUp_4 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_2 = null;
   
   //property nodes
   
   //method nodes
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_2 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_2 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_2 )
         {
            {
               uScript_Global component = event_UnityEngine_GameObject_Instance_2.GetComponent<uScript_Global>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_2.AddComponent<uScript_Global>();
               }
               if ( null != component )
               {
                  component.uScriptStart += Instance_uScriptStart_2;
                  component.uScriptLateStart += Instance_uScriptLateStart_2;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_2 )
      {
         {
            uScript_Global component = event_UnityEngine_GameObject_Instance_2.GetComponent<uScript_Global>();
            if ( null != component )
            {
               component.uScriptStart -= Instance_uScriptStart_2;
               component.uScriptLateStart -= Instance_uScriptLateStart_2;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_0.SetParent(g);
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3.SetParent(g);
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_4.SetParent(g);
   }
   public void Awake()
   {
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_0.Loaded += uScriptAct_LoadLevel_Loaded_0;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.Loaded += uScriptAct_LoadLevel_Loaded_1;
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_0.Update( );
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.Update( );
   }
   
   public void OnDestroy()
   {
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_0.Loaded -= uScriptAct_LoadLevel_Loaded_0;
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.Loaded -= uScriptAct_LoadLevel_Loaded_1;
   }
   
   void Instance_uScriptStart_2(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptStart_2( );
   }
   
   void Instance_uScriptLateStart_2(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_uScriptLateStart_2( );
   }
   
   void uScriptAct_LoadLevel_Loaded_0(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_0( );
   }
   
   void uScriptAct_LoadLevel_Loaded_1(object o, System.EventArgs e)
   {
      //fill globals
      //relay event to nodes
      Relay_Loaded_1( );
   }
   
   void Relay_Loaded_0()
   {
   }
   
   void Relay_In_0()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_0.In(logic_uScriptAct_LoadLevel_name_0, logic_uScriptAct_LoadLevel_destroyOtherObjects_0, logic_uScriptAct_LoadLevel_blockUntilLoaded_0);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_Loaded_1()
   {
   }
   
   void Relay_In_1()
   {
      {
         {
         }
         {
         }
         {
         }
      }
      logic_uScriptAct_LoadLevel_uScriptAct_LoadLevel_1.In(logic_uScriptAct_LoadLevel_name_1, logic_uScriptAct_LoadLevel_destroyOtherObjects_1, logic_uScriptAct_LoadLevel_blockUntilLoaded_1);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      
   }
   
   void Relay_uScriptStart_2()
   {
      Relay_In_3();
      Relay_In_4();
   }
   
   void Relay_uScriptLateStart_2()
   {
   }
   
   void Relay_In_3()
   {
      {
         {
         }
      }
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3.In(logic_uScriptAct_OnInputEventFilter_KeyCode_3);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_3.KeyUp;
      
      if ( test_0 == true )
      {
         Relay_In_0();
      }
   }
   
   void Relay_In_4()
   {
      {
         {
         }
      }
      logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_4.In(logic_uScriptAct_OnInputEventFilter_KeyCode_4);
      
      //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
      bool test_0 = logic_uScriptAct_OnInputEventFilter_uScriptAct_OnInputEventFilter_4.KeyUp;
      
      if ( test_0 == true )
      {
         Relay_In_1();
      }
   }
   
}
