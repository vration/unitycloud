// Shader created with Shader Forge Beta 0.23 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.23;sub:START;pass:START;ps:lgpr:1,nrmq:1,limd:1,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,uamb:True,mssp:True,ufog:True,aust:True,igpj:True,qofs:0,lico:1,qpre:3,flbk:,rntp:2,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.1280277,fgcg:0.1953466,fgcb:0.2352941,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0;n:type:ShaderForge.SFN_Final,id:1,x:31477,y:32557|diff-5-OUT,normal-9595-OUT,emission-124-OUT,alpha-9867-A,clip-9247-A;n:type:ShaderForge.SFN_Tex2d,id:2,x:33583,y:31596,ptlb:Texture1,ntxv:0,isnm:False|UVIN-150-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:3,x:33379,y:32491,ptlb:Texture2,ntxv:0,isnm:False|UVIN-9702-OUT;n:type:ShaderForge.SFN_Tex2d,id:4,x:33335,y:33148,ptlb:Texture3,ntxv:0,isnm:False|UVIN-9152-UVOUT;n:type:ShaderForge.SFN_Add,id:5,x:31893,y:31847|A-41-OUT,B-39-OUT,C-11-OUT,D-9796-OUT;n:type:ShaderForge.SFN_Multiply,id:11,x:32981,y:33129|A-4-RGB,B-37-RGB;n:type:ShaderForge.SFN_Color,id:28,x:33254,y:31821,ptlb:Text1_color,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:35,x:33166,y:32589,ptlb:Text2_color,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:37,x:33162,y:33273,ptlb:Text3_color,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:39,x:32986,y:32437|A-9200-OUT,B-35-RGB;n:type:ShaderForge.SFN_Multiply,id:41,x:33005,y:31998|A-9180-OUT,B-28-RGB;n:type:ShaderForge.SFN_Multiply,id:104,x:32742,y:32169|A-41-OUT,B-108-RGB;n:type:ShaderForge.SFN_Color,id:108,x:33078,y:32181,ptlb:Text1_Emission,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:115,x:32780,y:32486|A-39-OUT,B-117-RGB;n:type:ShaderForge.SFN_Color,id:117,x:32986,y:32589,ptlb:Text2_Emission,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Add,id:124,x:31881,y:32113|A-104-OUT,B-115-OUT,C-132-OUT,D-9809-OUT;n:type:ShaderForge.SFN_Multiply,id:132,x:32739,y:33209|A-11-OUT,B-139-RGB;n:type:ShaderForge.SFN_Color,id:139,x:32981,y:33273,ptlb:Text3_Emission,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Rotator,id:150,x:33896,y:31594|UVIN-164-UVOUT,SPD-8742-OUT;n:type:ShaderForge.SFN_TexCoord,id:164,x:34347,y:31584,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:8742,x:34116,y:31761,ptlb:Text1_rot,v1:0;n:type:ShaderForge.SFN_Panner,id:8755,x:33990,y:32371,spu:-0.1,spv:0.1|UVIN-9121-UVOUT;n:type:ShaderForge.SFN_TexCoord,id:9121,x:34361,y:32319,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:9143,x:33910,y:33299,ptlb:Text3_rot,v1:2;n:type:ShaderForge.SFN_Rotator,id:9152,x:33714,y:33111|UVIN-9165-UVOUT,SPD-9143-OUT;n:type:ShaderForge.SFN_TexCoord,id:9165,x:34163,y:33101,uv:0;n:type:ShaderForge.SFN_Time,id:9178,x:33910,y:31323;n:type:ShaderForge.SFN_Frac,id:9179,x:33531,y:31323|IN-9188-OUT;n:type:ShaderForge.SFN_Multiply,id:9180,x:33254,y:31661|A-9179-OUT,B-2-RGB;n:type:ShaderForge.SFN_Slider,id:9187,x:33849,y:31462,ptlb:Text1_strobe,min:0,cur:3.595517,max:10;n:type:ShaderForge.SFN_Multiply,id:9188,x:33700,y:31323|A-9178-T,B-9187-OUT;n:type:ShaderForge.SFN_Multiply,id:9200,x:33166,y:32386|A-9203-OUT,B-3-RGB;n:type:ShaderForge.SFN_Frac,id:9203,x:33436,y:31928|IN-9206-OUT;n:type:ShaderForge.SFN_Multiply,id:9206,x:33594,y:31928|A-9209-T,B-9212-OUT;n:type:ShaderForge.SFN_Time,id:9209,x:33962,y:31761;n:type:ShaderForge.SFN_Slider,id:9212,x:33871,y:31967,ptlb:Text2_strobe,min:0,cur:1.654145,max:10;n:type:ShaderForge.SFN_Tex2d,id:9247,x:32354,y:32895,ptlb:Mask,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9533,x:32610,y:32646,ptlb:Normal,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Normalize,id:9546,x:32353,y:32635|IN-9533-RGB;n:type:ShaderForge.SFN_Multiply,id:9595,x:32161,y:32635|A-9546-OUT,B-9598-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9598,x:32353,y:32787,ptlb:Normal Value,v1:0;n:type:ShaderForge.SFN_Multiply,id:9652,x:33871,y:32181|A-9662-OUT,B-9729-OUT;n:type:ShaderForge.SFN_Frac,id:9662,x:34096,y:32055|IN-9671-TSL;n:type:ShaderForge.SFN_Time,id:9671,x:34370,y:31938;n:type:ShaderForge.SFN_Add,id:9702,x:33682,y:32287|A-9652-OUT,B-8755-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:9729,x:34143,y:32261,ptlb:Text2_Panspeed,v1:-1;n:type:ShaderForge.SFN_Tex2d,id:9782,x:32770,y:31649,ptlb:BaseTexture,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:9795,x:32878,y:31887,ptlb:BaseTextcolor,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:9796,x:32496,y:31832|A-9782-RGB,B-9795-RGB;n:type:ShaderForge.SFN_Multiply,id:9809,x:32286,y:31996|A-9796-OUT,B-9810-RGB;n:type:ShaderForge.SFN_Color,id:9810,x:32542,y:32054,ptlb:BaseTexture_emmision,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:9867,x:32085,y:32779,ptlb:Alpha,c1:0.5,c2:0.5,c3:0.5,c4:1;proporder:2-3-4-28-35-37-108-117-139-8742-9143-9187-9212-9247-9533-9598-9729-9782-9795-9810-9867;pass:END;sub:END;*/

Shader "Shader Forge/Graphic1" {
    Properties {
        _Texture1 ("Texture1", 2D) = "white" {}
        _Texture2 ("Texture2", 2D) = "white" {}
        _Texture3 ("Texture3", 2D) = "white" {}
        _Text1color ("Text1_color", Color) = (0,0,0,1)
        _Text2color ("Text2_color", Color) = (1,1,1,1)
        _Text3color ("Text3_color", Color) = (1,1,1,1)
        _Text1Emission ("Text1_Emission", Color) = (0.5,0.5,0.5,1)
        _Text2Emission ("Text2_Emission", Color) = (0.5,0.5,0.5,1)
        _Text3Emission ("Text3_Emission", Color) = (1,1,1,1)
        _Text1rot ("Text1_rot", Float ) = 0
        _Text3rot ("Text3_rot", Float ) = 2
        _Text1strobe ("Text1_strobe", Range(0, 10)) = 0
        _Text2strobe ("Text2_strobe", Range(0, 10)) = 0
        _Mask ("Mask", 2D) = "white" {}
        _Normal ("Normal", 2D) = "bump" {}
        _NormalValue ("Normal Value", Float ) = 0
        _Text2Panspeed ("Text2_Panspeed", Float ) = -1
        _BaseTexture ("BaseTexture", 2D) = "white" {}
        _BaseTextcolor ("BaseTextcolor", Color) = (1,1,1,1)
        _BaseTextureemmision ("BaseTexture_emmision", Color) = (0.5,0.5,0.5,1)
        _Alpha ("Alpha", Color) = (0.5,0.5,0.5,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _Texture1; uniform float4 _Texture1_ST;
            uniform sampler2D _Texture2; uniform float4 _Texture2_ST;
            uniform sampler2D _Texture3; uniform float4 _Texture3_ST;
            uniform float4 _Text1color;
            uniform float4 _Text2color;
            uniform float4 _Text3color;
            uniform float4 _Text1Emission;
            uniform float4 _Text2Emission;
            uniform float4 _Text3Emission;
            uniform float _Text1rot;
            uniform float _Text3rot;
            uniform float _Text1strobe;
            uniform float _Text2strobe;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _NormalValue;
            uniform float _Text2Panspeed;
            uniform sampler2D _BaseTexture; uniform float4 _BaseTexture_ST;
            uniform float4 _BaseTextcolor;
            uniform float4 _BaseTextureemmision;
            uniform float4 _Alpha;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float2 node_10281 = i.uv0;
                clip(tex2D(_Mask,TRANSFORM_TEX(node_10281.rg, _Mask)).a - 0.5);
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalLocal = (normalize(UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(node_10281.rg, _Normal))).rgb)*_NormalValue);
                float3 normalDirection =  mul( normalLocal, tangentTransform ); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor + UNITY_LIGHTMODEL_AMBIENT.xyz;
////// Emissive:
                float4 node_9178 = _Time + _TimeEditor;
                float4 node_10280 = _Time + _TimeEditor;
                float node_150_ang = node_10280.g;
                float node_150_spd = _Text1rot;
                float node_150_cos = cos(node_150_spd*node_150_ang);
                float node_150_sin = sin(node_150_spd*node_150_ang);
                float2 node_150_piv = float2(0.5,0.5);
                float2 node_150 = (mul(i.uv0.rg-node_150_piv,float2x2( node_150_cos, -node_150_sin, node_150_sin, node_150_cos))+node_150_piv);
                float3 node_41 = ((frac((node_9178.g*_Text1strobe))*tex2D(_Texture1,TRANSFORM_TEX(node_150, _Texture1)).rgb)*_Text1color.rgb);
                float4 node_9209 = _Time + _TimeEditor;
                float4 node_9671 = _Time + _TimeEditor;
                float2 node_9702 = ((frac(node_9671.r)*_Text2Panspeed)+(i.uv0.rg+node_10280.g*float2(-0.1,0.1)));
                float3 node_39 = ((frac((node_9209.g*_Text2strobe))*tex2D(_Texture2,TRANSFORM_TEX(node_9702, _Texture2)).rgb)*_Text2color.rgb);
                float node_9152_ang = node_10280.g;
                float node_9152_spd = _Text3rot;
                float node_9152_cos = cos(node_9152_spd*node_9152_ang);
                float node_9152_sin = sin(node_9152_spd*node_9152_ang);
                float2 node_9152_piv = float2(0.5,0.5);
                float2 node_9152 = (mul(i.uv0.rg-node_9152_piv,float2x2( node_9152_cos, -node_9152_sin, node_9152_sin, node_9152_cos))+node_9152_piv);
                float3 node_11 = (tex2D(_Texture3,TRANSFORM_TEX(node_9152, _Texture3)).rgb*_Text3color.rgb);
                float3 node_9796 = (tex2D(_BaseTexture,TRANSFORM_TEX(node_10281.rg, _BaseTexture)).rgb*_BaseTextcolor.rgb);
                float3 emissive = ((node_41*_Text1Emission.rgb)+(node_39*_Text2Emission.rgb)+(node_11*_Text3Emission.rgb)+(node_9796*_BaseTextureemmision.rgb));
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                finalColor += diffuseLight * (node_41+node_39+node_11+node_9796);
                finalColor += emissive;
/// Final Color:
                return fixed4(finalColor,_Alpha.a);
            }
            ENDCG
        }
        Pass {
            Name "ForwardAdd"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            ZWrite Off
            
            Fog { Color (0,0,0,0) }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _Texture1; uniform float4 _Texture1_ST;
            uniform sampler2D _Texture2; uniform float4 _Texture2_ST;
            uniform sampler2D _Texture3; uniform float4 _Texture3_ST;
            uniform float4 _Text1color;
            uniform float4 _Text2color;
            uniform float4 _Text3color;
            uniform float4 _Text1Emission;
            uniform float4 _Text2Emission;
            uniform float4 _Text3Emission;
            uniform float _Text1rot;
            uniform float _Text3rot;
            uniform float _Text1strobe;
            uniform float _Text2strobe;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform float _NormalValue;
            uniform float _Text2Panspeed;
            uniform sampler2D _BaseTexture; uniform float4 _BaseTexture_ST;
            uniform float4 _BaseTextcolor;
            uniform float4 _BaseTextureemmision;
            uniform float4 _Alpha;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 binormalDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(_Object2World, v.vertex);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float2 node_10283 = i.uv0;
                clip(tex2D(_Mask,TRANSFORM_TEX(node_10283.rg, _Mask)).a - 0.5);
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
/////// Normals:
                float3 normalLocal = (normalize(UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(node_10283.rg, _Normal))).rgb)*_NormalValue);
                float3 normalDirection =  mul( normalLocal, tangentTransform ); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = dot( normalDirection, lightDirection );
                float3 diffuse = max( 0.0, NdotL) * attenColor;
                float3 finalColor = 0;
                float3 diffuseLight = diffuse;
                float4 node_9178 = _Time + _TimeEditor;
                float4 node_10282 = _Time + _TimeEditor;
                float node_150_ang = node_10282.g;
                float node_150_spd = _Text1rot;
                float node_150_cos = cos(node_150_spd*node_150_ang);
                float node_150_sin = sin(node_150_spd*node_150_ang);
                float2 node_150_piv = float2(0.5,0.5);
                float2 node_150 = (mul(i.uv0.rg-node_150_piv,float2x2( node_150_cos, -node_150_sin, node_150_sin, node_150_cos))+node_150_piv);
                float3 node_41 = ((frac((node_9178.g*_Text1strobe))*tex2D(_Texture1,TRANSFORM_TEX(node_150, _Texture1)).rgb)*_Text1color.rgb);
                float4 node_9209 = _Time + _TimeEditor;
                float4 node_9671 = _Time + _TimeEditor;
                float2 node_9702 = ((frac(node_9671.r)*_Text2Panspeed)+(i.uv0.rg+node_10282.g*float2(-0.1,0.1)));
                float3 node_39 = ((frac((node_9209.g*_Text2strobe))*tex2D(_Texture2,TRANSFORM_TEX(node_9702, _Texture2)).rgb)*_Text2color.rgb);
                float node_9152_ang = node_10282.g;
                float node_9152_spd = _Text3rot;
                float node_9152_cos = cos(node_9152_spd*node_9152_ang);
                float node_9152_sin = sin(node_9152_spd*node_9152_ang);
                float2 node_9152_piv = float2(0.5,0.5);
                float2 node_9152 = (mul(i.uv0.rg-node_9152_piv,float2x2( node_9152_cos, -node_9152_sin, node_9152_sin, node_9152_cos))+node_9152_piv);
                float3 node_11 = (tex2D(_Texture3,TRANSFORM_TEX(node_9152, _Texture3)).rgb*_Text3color.rgb);
                float3 node_9796 = (tex2D(_BaseTexture,TRANSFORM_TEX(node_10283.rg, _BaseTexture)).rgb*_BaseTextcolor.rgb);
                finalColor += diffuseLight * (node_41+node_39+node_11+node_9796);
/// Final Color:
                return fixed4(finalColor * _Alpha.a,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCollector"
            Tags {
                "LightMode"="ShadowCollector"
            }
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCOLLECTOR
            #define SHADOW_COLLECTOR_PASS
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcollector
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_COLLECTOR;
                float4 uv0 : TEXCOORD5;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_COLLECTOR(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float2 node_10284 = i.uv0;
                clip(tex2D(_Mask,TRANSFORM_TEX(node_10284.rg, _Mask)).a - 0.5);
                SHADOW_COLLECTOR_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Cull Off
            Offset 1, 1
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float2 node_10285 = i.uv0;
                clip(tex2D(_Mask,TRANSFORM_TEX(node_10285.rg, _Mask)).a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
