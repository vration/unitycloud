﻿using UnityEngine;
using System.Collections;

public class OnlyAllowOne : MonoBehaviour 
{
	private void Awake()
	{
		tag = "OnlyAllowOne";

		if( GameObject.FindGameObjectsWithTag( "OnlyAllowOne" ).Length > 1 )
		{
			Destroy( gameObject );
		}
	}
}
