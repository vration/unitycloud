﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof ( Animator ))]
public class SpotlightController : MonoBehaviour 
{
	private void Start()
	{
		_Animator = GetComponent<Animator>();
		//_Animator.speed = .01f;

		if( _LightColors.Length > 0 )
		{
			ChangeAllLightColors( 0 );
		}

		StopAnim();
	}

	public void PlayAnim( string inClipName )
	{
		Debug.Log( "PlayAnim: " + inClipName );

		gameObject.SetActive( true );
		_Animator.Play( inClipName );
	}

	public void StopAnim()
	{
		gameObject.SetActive( false );
	}

	//called from Animation Event in timeline
	public void ChooseNextColor()
	{
		if( _ColorIndex < _LightColors.Length - 1 )
		{
			++_ColorIndex;
		}
		else
		{
			_ColorIndex = 0;
		}
	}

	//called from Animation Event in timeline
	public void SetAnimationSpeed( float inSpeed )
	{
		_Animator.speed = inSpeed;
	}

	//called from Animation Event in timeline
	public void SetColor( int inColorIndex )
	{
		if( inColorIndex < _LightColors.Length )
		{
			_ColorIndex = inColorIndex;
		}
		else
		{
			Debug.LogError( "CANNOT SetColor to index #" + inColorIndex + " because there are only " + _LightColors.Length + " colors, fag!" );
		}
	}

	//called from Animation Event in timeline
	public void ChangeLightColor( int inLightIndex )
	{
		SetLightColor( inLightIndex, _LightColors[ _ColorIndex ] );
	}

	//called from Animation Event in timeline
	public void ChangeAllLightColors( int inColorIndex )
	{
		if( inColorIndex < 0 )
		{
			inColorIndex = _ColorIndex;//use negative number to go to next selected color
		}

		//Debug.Log( "ChangeAllLightColors( "+inColorIndex+" )" );

		ChangeAllLightsToColor( _LightColors[ inColorIndex ] );
	}

	//called from Animation Event in timeline
	public void IncrementAllLightColors()
	{
		ChooseNextColor();
		ChangeAllLightsToColor( _LightColors[ _ColorIndex ] );
	}

	//called from Animation Event in timeline
	public void StrobeAllLights( float inStrobeRate )
	{
		_StrobeRate = inStrobeRate;
		_StartStrobeTime = Time.time;
	}

	//called from Animation Event in timeline
	public void StopStrobe( string inKeepLightsOn )
	{
		_StartStrobeTime = -1;

		if( inKeepLightsOn.ToLower() == "on" )
		{
			ChangeAllLightsToColor( _LightColors[ _ColorIndex ] );
		}
		else
		{
			ChangeAllLightsToColor( new Color( 0,0,0,0 ) );//off
		}
	}

	public float GetCurrentAnimationLength()
	{
		return _Animator.GetCurrentAnimatorStateInfo( 0 ).length;
	}

	private void FixedUpdate()
	{
		if( _StartStrobeTime >= 0 )
		{
			float elapsedTime = Time.time - _StartStrobeTime;
			
			if( elapsedTime > _StrobeRate )
			{
				_StartStrobeTime = Time.time;
				
				if( _StrobeOn )
				{
					_StrobeOn = false;
					//ChangeAllLightsToColor( new Color( 0,0,0,0 ) );//off

					SetAllLightsEnabled( false );
				}
				else
				{
					_StrobeOn = true;
					//ChangeAllLightsToColor( _LightColors[ _ColorIndex ] );

					SetAllLightsEnabled( true );
				}
			}
		}
	}

	private void ChangeAllLightsToColor( Color inColor )
	{
		for( int i = 0; i < _Lights.Length; ++i )
		{
			SetLightColor( i, inColor );
		}
	}

	private void SetAllLightsEnabled( bool inEnabled )
	{
		for( int i = 0; i < _Lights.Length; ++i )
		{
			_Lights[ i ].enabled = inEnabled;
		}
	}

	private void SetLightColor( int inLightIndex, Color inColor )
	{
		if( inLightIndex < _Lights.Length )
		{
			//_Lights[ inLightIndex ].renderer.material.color = inColor;
			_Lights[ inLightIndex ].material.SetColor( "_ConeColor", inColor );
		}
		else
		{
			Debug.LogError( "CANNOT ChangeLightColor of light #" + inLightIndex + " because there are only " + _Lights.Length + " lights, fag!" );
		}
	}

	private	int							_ColorIndex = 0;
	private bool						_StrobeOn;
	private float						_StartStrobeTime = -1;
	private float						_StrobeRate = 1;
	private Animator					_Animator = null;
	private List<Color>					_CachedLightColors = new List<Color>();
	
	[SerializeField] MeshRenderer[]		_Lights;
	[SerializeField] Color[]			_LightColors;
}
