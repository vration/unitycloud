﻿using UnityEngine;
using System.Collections;

public class PreserveController : MonoBehaviour 
{
	private void Awake()
	{
		DontDestroyOnLoad( gameObject );
	}
}
