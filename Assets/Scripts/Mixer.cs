﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Mixer : MonoBehaviour 
{
	public void TrebleSliderChanged( float inValue )
	{
		Debug.Log( "TrebleSliderChanged " + inValue );
	}

	public void MidSliderChanged( float inValue )
	{
		Debug.Log( "MidSliderChanged " + inValue );
	}

	public void BassSliderChanged( float inValue )
	{
		Debug.Log( "BassSliderChanged " + inValue );
	}

	public void VolumeSliderChanged( float inValue )
	{
		Debug.Log( "VolumeSliderChanged " + inValue );
	}
}