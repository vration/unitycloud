﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ButtonBar : MonoBehaviour 
{
	public int 					BarIndex = 0;
	public SequencerMode 		SequencerMode;

	private void Start()
	{
		_AudioBank = GetComponent<AudioBank>();
		_VisualBank = GetComponent<VisualBank>();

		InitButtonColors();

		_SelectedButton = _Buttons[0].GetComponent<SequencerButton>();
		_CurrentlyActiveButton = _SelectedButton;
		_SelectedButton.Select();//activate kill button
	}

	public void HandleButtonPress( GameObject inGO, SequencerMode inMode )
	{
		Debug.Log( "Press " + inGO.name + " Mode = " + inMode );

		//deselect any possibly blinking buttons
		if( _SelectedButton != _CurrentlyActiveButton )
		{
			_SelectedButton.Deselect();
		}
		
		//select new button
		_SelectedButton = inGO.GetComponent<SequencerButton>();

		if( _SelectedButton.ButtonState == ButtonState.Deselected || _SelectedButton.ButtonState == ButtonState.FlaggedToStop )
		{
			_SelectedButton.QueueToPlay();
		}
		else if( _SelectedButton.ButtonState == ButtonState.Playing || _SelectedButton.ButtonState == ButtonState.QueuedToPlay )
		{
			if( _SelectedButton.Index == 0 )
			{
				return;
			}
			else
			{
				_SelectedButton.FlagToStop();
			}
		}

		if( SequencerMode == SequencerMode.Visual )
		{
			if( _SelectedButton.Index == 0 ) //kill button
			{
				_VisualBank.QueueClipToStop();
			}
			else if( _SelectedButton == _CurrentlyActiveButton ) ///same button pressed second time
			{
				if( _VisualBank.PlayingClip == null )
				{
					_VisualBank.SetQueuedClip( _SelectedButton.Index - 1 );
				}
				else
				{
					_VisualBank.QueueClipToStop();
				}
			}
			else
			{
				_VisualBank.SetQueuedClip( _SelectedButton.Index - 1 );
			}
		}
		else ///audio bank
		{
			if( _SelectedButton.Index == 0 ) //kill button
			{
				_AudioBank.QueueClipToStop();
			}
			else if( _SelectedButton == _CurrentlyActiveButton ) ///same button pressed second time
			{
				if( _AudioBank.PlayingClip == null )
				{
					_AudioBank.SetQueuedClip( _SelectedButton.Index - 1 );
				}
				else
				{
					_AudioBank.QueueClipToStop();
				}
			}
			else
			{
				_AudioBank.SetQueuedClip( _SelectedButton.Index - 1 );
			}
		}
	}

	public void SelectButton()
	{
		//Debug.Log( "ButtonBar.SelectButton()");

		if( _SelectedButton != _CurrentlyActiveButton )
		{
			if( _CurrentlyActiveButton != null )
			{
				_CurrentlyActiveButton.Deselect();
			}
			_SelectedButton.Select();
			_CurrentlyActiveButton = _SelectedButton;
		}

		SequencerButton offButton = _Buttons[0].GetComponent<SequencerButton>();
		if( _SelectedButton != offButton )
		{
			offButton.Deselect();
		}
	}

	public void DeselectButton()
	{
		//Debug.Log( "ButtonBar.DeselectButton()");

		//deselect active
		if( _CurrentlyActiveButton != null )
		{
			_CurrentlyActiveButton.Deselect();
		}
		//select new button
		if( _SelectedButton != _CurrentlyActiveButton )
		{
			_SelectedButton.Select();
			_CurrentlyActiveButton = _SelectedButton;
		}
		else
		{
			SequencerButton offButton = _Buttons[0].GetComponent<SequencerButton>();
			if( _SelectedButton != offButton )
			{
				offButton.Select();
			}
		}
	}

	private void InitButtonColors()
	{
		for( int i = 0; i < _Buttons.Count; ++i )
		{
			SequencerButton s = _Buttons[i].GetComponent<SequencerButton>();

			if( i == 0 ) //kill button
			{
				s.Init( i, _DefaultKillButtonColor, _HoverKillButtonColor, _SelectedKillButtonColor, _SelectedKillHoverButtonColor );
			}
			else
			{
				s.Init( i, _DefaultButtonColor, _HoverButtonColor, _SelectedButtonColor, _SelectedHoverButtonColor );
			}
		}
	}

	private SequencerButton						_CurrentlyActiveButton = null;
	private SequencerButton 					_SelectedButton = null;
	private AudioBank							_AudioBank = null;
	private VisualBank							_VisualBank = null;

	[SerializeField] private List<GameObject> 	_Buttons = new List<GameObject>();
	
	[SerializeField] Color						_DefaultButtonColor;
	[SerializeField] Color						_HoverButtonColor;
	[SerializeField] Color						_SelectedButtonColor;
	[SerializeField] Color						_SelectedHoverButtonColor;
	
	[SerializeField] Color		 				_DefaultKillButtonColor;
	[SerializeField] Color		 				_HoverKillButtonColor;
	[SerializeField] Color		 				_SelectedKillButtonColor;
	[SerializeField] Color		 				_SelectedKillHoverButtonColor;
}