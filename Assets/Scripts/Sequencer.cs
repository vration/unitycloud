﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum SequencerMode
{
	Audio,
	Visual
}

public enum Preset
{
	House,
	Techno,
	Breaks
}

public class Sequencer : MonoBehaviour 
{
	public static Sequencer 	Instance = null;
	public static Preset		SelectedPreset;

	[HideInInspector] public bool	IsHidden = true;

	private void Awake() 
	{
		Instance = this;
	}

	private void Start()
	{
		_StartTime = Time.time;

		SelectedPreset = Preset.House;

		_SelectedPresetButton = _PresetButtons[0];
		_SelectedPresetButton.GetComponent<UIButton>().enabled = false;
		_SelectedPresetButton.GetComponent<UISprite>().color = _SelectedPresetColor;

		_FirstPersonController = GameObject.FindGameObjectWithTag( "FPC" );
		_MainStageAudio = GameObject.Find( "MainstageAudio" );

		//init tween positions
		float yPos = _Background.transform.localPosition.y;
		float height = (float) _Background.height;
		_TweenPosition.to = new Vector3( 0, yPos, 0 );
		_TweenPosition.from = new Vector3( 0, yPos - height - 20f, 0 );

		_TweenPosition.SetOnFinished( TweenComplete );
	}

	private void FixedUpdate()
	{
		float elapsedTime = Time.time - _StartTime;
		int newBeat = (int) ( elapsedTime / ( 60 / _BPM ) ) % _CycleLength;

		if( newBeat != _CurrentBeat )
		{
			_CurrentBeat = newBeat;
			BeatCounter.Instance.SetBeat( _CurrentBeat );

			float barLength = ( 60 / _BPM ) * 4;
		
			//check length of cued clips and play 1 bar clips on bar start, 4 bar clips on cycle restart
			for( int i = 0; i < _AudioBanks.Count; ++i )
			{
				AudioBank audioBank = _AudioBanks[i];
				ButtonBar buttonBar = _ButtonBars[i + 4];

				if( audioBank.QueuedClip != null )
				{
					//decide when to start playing next clip
					float playingClipLength = 0;
					if( audioBank.PlayingClip != null )
					{
						playingClipLength = (float) audioBank.PlayingClip.samples / audioBank.PlayingClip.frequency;
					}
					float queuedClipLength = (float) audioBank.QueuedClip.samples / audioBank.QueuedClip.frequency;

					float clipLength = Mathf.Max( playingClipLength, queuedClipLength );

					Debug.Log ( "CASE 1: queuedClip = " + audioBank.QueuedClip + "   clipLength = " + clipLength + "   barLength = " + barLength );
					if( clipLength <= barLength  * 1.2f ) //1 bar clip
					{
						if( _CurrentBeat % ( (float) _CycleLength / 4 ) == 0 )
						{
							Debug.Log ( "PLAY 1 bar clip" );
							audioBank.PlayQueuedClip();
							buttonBar.SelectButton();
						}
					}
					else if( clipLength <= ( barLength * 2 )  * 1.2f ) //2 bar clip
					{
						if( _CurrentBeat % ( (float) _CycleLength / 2 ) == 0 )
						{
							Debug.Log ( "PLAY 2 bar clip" );
							audioBank.PlayQueuedClip();
							buttonBar.SelectButton();
						}
					}
					else if( clipLength > ( barLength * 3 ) ) //4 bar clip
					{
						if( _CurrentBeat % ( (float) _CycleLength ) == 0 )
						{
							Debug.Log ( "PLAY 4 bar clip" );
							audioBank.PlayQueuedClip();
							buttonBar.SelectButton();
						}
					}
				}
				else if( audioBank.FlagToStopClip )
				{
					Debug.Log ( "CASE 2: audioBank = " + audioBank + "   PlayingClip = " + audioBank.PlayingClip );

					float playingClipLength = 0;
					float queuedClipLength = 0;
					if( audioBank.PlayingClip != null )
					{
						playingClipLength = (float) audioBank.PlayingClip.samples / audioBank.PlayingClip.frequency;
					}
					if( audioBank.QueuedClip != null )
					{
						queuedClipLength = (float) audioBank.QueuedClip.samples / audioBank.QueuedClip.frequency;
					}
					
					float clipLength = Mathf.Max( playingClipLength, queuedClipLength );

					//float clipLength = (float) audioBank.PlayingClip.samples / audioBank.PlayingClip.frequency;

					Debug.Log ( "PlayingClip = " + audioBank.PlayingClip + "   queuedClipLength = " + queuedClipLength + "   barLength = " + barLength );
					if( clipLength <= barLength * 1.2f ) //1 bar clip
					{
						if( _CurrentBeat % ( (float) _CycleLength / 4 ) == 0 )
						{
							Debug.Log ( "STOP 1 bar clip" );
							audioBank.StopClip();
							buttonBar.DeselectButton();
						}

					}
					else if( clipLength <= ( barLength * 2 ) * 1.2f ) //2 bar clip
					{
						if( _CurrentBeat % ( (float) _CycleLength / 2 ) == 0 )
						{
							Debug.Log ( "STOP 2 bar clip" );
							audioBank.StopClip();
							buttonBar.DeselectButton();
						}
					}
					else if( clipLength > ( barLength * 3 ) ) //4 bar clip
					{
						if( _CurrentBeat % ( (float) _CycleLength ) == 0 )
						{
							Debug.Log ( "STOP 4 bar clip" );
							audioBank.StopClip();
							buttonBar.DeselectButton();
						}
					}
				}//audio

				//check length of cued visual clips and play 1 bar clips on bar start, 4 bar clips on cycle restart
				for( int j = 0; j < _VisualBanks.Count; ++j )
				{
					VisualBank visualBank = _VisualBanks[j];
					buttonBar = _ButtonBars[j];
					string queuedClipString = visualBank.QueuedClip;
					
					if( ! string.IsNullOrEmpty( queuedClipString ) )
					{
						float clipLength = 3.33f;
						
						Debug.Log ( "queuedClipString = " + queuedClipString + "   clipLength = " + clipLength + "   barLength = " + barLength );
						if( clipLength <= barLength + .02f ) //1 bar clip
						{
							if( _CurrentBeat % ( (float) _CycleLength / 4 ) == 0 )
							{
								Debug.Log ( "PLAY 1 bar clip" );
								visualBank.PlayQueuedClip();
								buttonBar.SelectButton();
							}
						}
						else if( clipLength <= ( barLength * 2 ) + .02f ) //2 bar clip
						{
							if( _CurrentBeat % ( (float) _CycleLength / 2 ) == 0 )
							{
								Debug.Log ( "PLAY 2 bar clip" );
								visualBank.PlayQueuedClip();
								buttonBar.SelectButton();
							}
						}
						else if( clipLength > ( barLength * 3 ) ) //4 bar clip
						{
							if( _CurrentBeat % ( (float) _CycleLength ) == 0 )
							{
								Debug.Log ( "PLAY 4 bar clip" );
								visualBank.PlayQueuedClip();
								buttonBar.SelectButton();
							}
						}
					}
					else if( visualBank.FlagToStopClip )
					{
						Debug.Log ( "::::: visualBank = " + visualBank + "   PlayingClip = " + visualBank.PlayingClip );
						
						
						float clipLength = 3.33f;
						
						Debug.Log ( "PlayingClip = " + visualBank.PlayingClip + "   clipLength = " + clipLength + "   barLength = " + barLength );
						if( clipLength <= barLength + .02f ) //1 bar clip
						{
							if( _CurrentBeat % ( (float) _CycleLength / 4 ) == 0 )
							{
								Debug.Log ( "STOP 1 bar clip" );
								visualBank.StopClip();
								buttonBar.DeselectButton();
							}
							
						}
						else if( clipLength <= ( barLength * 2 ) + .02f ) //2 bar clip
						{
							if( _CurrentBeat % ( (float) _CycleLength / 2 ) == 0 )
							{
								Debug.Log ( "STOP 2 bar clip" );
								visualBank.StopClip();
								buttonBar.DeselectButton();
							}
						}
						else if( clipLength > ( barLength * 3 ) ) //4 bar clip
						{
							if( _CurrentBeat % ( (float) _CycleLength ) == 0 )
							{
								Debug.Log ( "STOP 4 bar clip" );
								visualBank.StopClip();
								buttonBar.DeselectButton();
							}
						}
					}//visual
				}
			}
		}
	}

	public void Toggle()
	{
		if( IsHidden )
		{
			_Panel.gameObject.SetActive( true );

			ActivateSequencerMode();
		}
		else
		{
			DeactivateSequencerMode();
		}

		_TweenPosition.Toggle();
	}
	
	public void ActivateSequencerMode()
	{
		IsHidden = false;

		_Panel.alpha = 1;

		//store world position
		_LastFPCParentTransform = _FirstPersonController.transform.parent;
		_LastFPCPosition = _FirstPersonController.transform.localPosition;
		_LastFPCRotation = _FirstPersonController.transform.localRotation;

		//go to stage
		_FirstPersonController.transform.parent = _BoothLocationTransform;
		_FirstPersonController.transform.localPosition = Vector3.zero;
		_FirstPersonController.transform.localRotation = Quaternion.identity;

		_FirstPersonController.GetComponent<CharacterController>().enabled = false;
		_FirstPersonController.SendMessage( "SetDisabled" );//fucking JavaScript
		_MainStageAudio.SetActive( false );
		_OverheadSpotlights.SetActive( false );
		_MainStageSpotlights.SetActive( false );
	}
	
	public void DeactivateSequencerMode()
	{
		IsHidden = true;

		//reset to world position
		_FirstPersonController.transform.parent = _LastFPCParentTransform;
		_FirstPersonController.transform.localPosition = _LastFPCPosition;
		_FirstPersonController.transform.localRotation = _LastFPCRotation;

		_FirstPersonController.GetComponent<CharacterController>().enabled = true;
		_FirstPersonController.SendMessage( "SetEnabled" );//fucking JavaScript
		_MainStageAudio.SetActive( true );
		_OverheadSpotlights.SetActive( true );
		_MainStageSpotlights.SetActive( true );
	}

	private void TweenComplete()
	{
		//Debug.Log( "TweenComplete()   IsHidden = " + IsHidden );
		
		if( IsHidden )
		{
			_Panel.gameObject.SetActive( false );
		}
	}

	public void HandlePresetButtonPress( GameObject inGO )
	{
		//Debug.Log( "HandlePresetButtonPress " + inGO.name );

		//deselect old
		UIButton b = _SelectedPresetButton.GetComponent<UIButton>();
		b.enabled = true;
		_SelectedPresetButton.GetComponent<UISprite>().color = b.defaultColor;

		//select new button
		_SelectedPresetButton = inGO;
		_SelectedPresetButton.GetComponent<UIButton>().enabled = false;
		_SelectedPresetButton.GetComponent<UISprite>().color = _SelectedPresetColor;

		if( inGO == _PresetButtons[0] )
		{
			SelectedPreset = Preset.House;
			_TrackTempo = 128;
		}
		else if( inGO == _PresetButtons[1] )
		{
			SelectedPreset = Preset.Techno;
			_TrackTempo = 144;
		}
		else
		{
			SelectedPreset = Preset.Breaks;
			_TrackTempo = 208;
		}

		ResetSequencer();
	}

	private void ResetSequencer()
	{
		//stop all clips

		//reset banks to off button

		//reset beat counter
		BeatCounter.Instance.Reset();

		_StartTime = Time.time;
		_BPM = _TrackTempo;
	}

	private int							_CurrentBeat = 0;
	private int							_CycleLength = 16;
	private float						_StartTime = 0;
	private float						_BPM = 128;//120.888165f;//128f
	private float 						_TrackTempo = 128;
	//private float						_BarLength = 1.875f;
	//private float						_BeatInterval = .46875f;//1.875f * 4;
	private GameObject 					_SelectedPresetButton = null;

	private GameObject					_FirstPersonController = null;
	private Transform					_LastFPCParentTransform = null;
	private Vector3						_LastFPCPosition = Vector3.zero;
	private Quaternion					_LastFPCRotation = Quaternion.identity;
	private GameObject					_MainStageAudio = null;
	
	[SerializeField] private UIPanel			_Panel = null;
	[SerializeField] private UIWidget			_Background = null;
	[SerializeField] private TweenPosition		_TweenPosition = null;
	[SerializeField] private Color		 		_SelectedPresetColor;
	[SerializeField] private GameObject			_OverheadSpotlights = null;
	[SerializeField] private GameObject			_MainStageSpotlights = null;
	[SerializeField] private Transform			_BoothLocationTransform = null;
	[SerializeField] private GameObject[]  		_PresetButtons = new GameObject[3];
	[SerializeField] private List<ButtonBar>	_ButtonBars = new List<ButtonBar>();
	[SerializeField] private List<VisualBank>	_VisualBanks = new List<VisualBank>();
	[SerializeField] private List<AudioBank>	_AudioBanks = new List<AudioBank>();
}